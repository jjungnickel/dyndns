package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/coreos/go-systemd/activation"
	"github.com/digitalocean/godo"
	"golang.org/x/oauth2"
)

var (
	listenAddr string
	authToken  string
	domainName string
	recordName string
	dryRun     bool
	token      string
)

type TokenSource struct {
	AccessToken string
}

func (t *TokenSource) Token() (*oauth2.Token, error) {
	token := &oauth2.Token{AccessToken: t.AccessToken}
	return token, nil
}

type Options struct {
	V4Address string `url:"ipaddr,omitempty"`
	V6Address string `url:"ip6addr,omitempty"`
}

func requestToken(r *http.Request) string {
	hdr := strings.Split(r.Header.Get("Authorization"), "")

	// bearer token auth
	if len(hdr) == 2 && hdr[0] == "Bearer" {
		return hdr[1]
	}

	// basic auth (username: ignore, password: token)
	_, token, ok := r.BasicAuth()

	if ok {
		return token
	}
	// if all else fails, use 'token' query string
	return r.URL.Query().Get("token")

}

func main() {
	flag.StringVar(&listenAddr, "listen-addr", ":8080", "server listen address")
	flag.StringVar(&token, "token", "", "require the value of token in request (bearer or query-string 'token')")
	flag.StringVar(&authToken, "do-token", "", "personal authentication token for digital-ocean")
	flag.StringVar(&domainName, "domain", "", "domain that holds the records to update")
	flag.StringVar(&recordName, "record", "", "record name within <domain> that should be updated")
	flag.BoolVar(&dryRun, "dry-run", false, "don't actually change anything, just print what would happen")
	flag.Parse()

	if domainName == "" {
		panic("-domain is required")
	}
	if authToken == "" {
		panic("-auth-token is required")
	}
	if recordName == "" {
		panic("-record is required")
	}
	var listener net.Listener
	if os.Getenv("LISTEN_PID") == strconv.Itoa(os.Getpid()) {
		listener = listenSocketActivation()
	} else {
		listener = listenPlain()
	}

	tokenSource := &TokenSource{AccessToken: authToken}
	oauthClient := oauth2.NewClient(context.Background(), tokenSource)
	client := godo.NewClient(oauthClient)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		rip, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			log.Printf("remote-address: %s is not host:port format", r.RemoteAddr)
		}
		remoteAddr := net.ParseIP(rip)

		// when -token is set, expect the same value in request
		if token != "" && token != requestToken(r) {
			w.WriteHeader(http.StatusForbidden)
			log.Printf("%s %s %d", remoteAddr, r.URL, http.StatusForbidden)
			return
		}

		var (
			v4Addr net.IP
			v6Addr net.IP
		)
		// prefer values from query
		v4Addr = net.ParseIP(r.URL.Query().Get("ipaddr"))
		v6Addr = net.ParseIP(r.URL.Query().Get("ip6addr"))

		// with no v4 address in query, try remote-address
		if v4Addr == nil && remoteAddr.To4() != nil {
			v4Addr = remoteAddr
		}
		// with no v6 address in query, try remote-address
		if v6Addr == nil && remoteAddr.To4() == nil {
			v6Addr = remoteAddr
		}
		records, _, err := client.Domains.Records(context.TODO(), domainName, nil)
		if err != nil {
			panic(fmt.Sprintf("cannot list records: %s", err))
		}
		for _, record := range records {
			if record.Name == recordName {
				if record.Type == "A" {
					updateRecord(client, record, v4Addr)
				}
				if record.Type == "AAAA" {
					updateRecord(client, record, v6Addr)
				}
			}
		}

		_, _ = w.Write([]byte("Thanks!"))
		log.Printf("%s %s %d", remoteAddr, r.URL, http.StatusOK)
	})

	err := http.Serve(listener, nil)

	if err != nil {
		log.Panicf("cannot serve: %s", err)
	}
}

func updateRecord(client *godo.Client, record godo.DomainRecord, addr net.IP) {
	if addr != nil {
		if dryRun {
			log.Printf("%s.%s: would set %s record to %s (dry-run)", record.Name, domainName, record.Type, addr.String())
		} else {
			log.Printf("%s.%s: setting %s record to %s", record.Name, domainName, record.Type, addr.String())
			_, _, err := client.Domains.EditRecord(context.TODO(), domainName, record.ID, &godo.DomainRecordEditRequest{
				Data: addr.String(),
			})
			if err != nil {
				panic(fmt.Sprintf("cannot edit %s record %d of domain %s: %s", record.Type, record.ID, domainName, err))
			}
		}

	} else {
		log.Printf("%s.%s: skip setting %s record because addr is nil", record.Name, domainName, record.Type)
	}

}
func listenSocketActivation() net.Listener {
	listeners, err := activation.Listeners()
	if err != nil {
		log.Panicf("cannot listen: %s", err)
	}
	if len(listeners) != 1 {
		log.Panicf("unexpected number of socket activation (%d != 1)", len(listeners))
	}
	return listeners[0]
}

func listenPlain() net.Listener {
	listener, err := net.Listen("tcp", listenAddr)
	if err != nil {
		log.Panicf("cannot listen: %s", err)
	}
	return listener
}
