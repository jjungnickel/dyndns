module dyndns

require (
	github.com/coreos/go-systemd v0.0.0-20181031085051-9002847aa142
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/digitalocean/godo v1.7.2
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	github.com/tent/http-link-go v0.0.0-20130702225549-ac974c61c2f9 // indirect
	golang.org/x/net v0.0.0-20181213202711-891ebc4b82d6 // indirect
	golang.org/x/oauth2 v0.0.0-20181203162652-d668ce993890
	golang.org/x/sync v0.0.0-20181108010431-42b317875d0f // indirect
	google.golang.org/appengine v1.3.0 // indirect
)
